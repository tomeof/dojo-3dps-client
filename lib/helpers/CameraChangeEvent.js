define([
  "dojo/topic"
], function (topic) {

  let _time = Date.now();

  topic.subscribe("CameraChanged", function (event) {
    _time = event.time;
  });

  return {

    getTime: function () {
      return _time;
    }

  };

});
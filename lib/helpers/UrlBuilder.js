define([
  "dojo/_base/declare"
], function (declare) {

  return declare(null, {

    constructor: function (protocol, host) {
      this._protocol = protocol;
      this._host = host;
      this._port = null;
      this._path = null;
      this._params = [];

      this.port = function (port) {
        this._port = port;
        return this;
      };

      this.path = function (path) {
        this._path = path;
        return this;
      };

      this.param = function (name, value) {
        this._params.push(`${name}=${value}&`);
        return this;
      };

      this.optional = function (name, value) {
        if (value) {
          this._params.push(`${name}=${value}&`);
        }
        return this;
      }

      this.build = function () {
        let result = `${this._protocol}://${this._host}`;
        if (this._port) { result += `:${this._port}`; }
        if (this._path) { result += `/${this._path}`; }
        if (this._params) {
          result += "?" + this._params.join("");
        }
        return result.substring(0, result.length - 1);
      }

    }

  });

});
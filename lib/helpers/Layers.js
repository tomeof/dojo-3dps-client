define([
  "dojo/_base/declare",
  "dojox/xml/DomParser",
], function (declare, DomParser) {

  return declare(null, {

    constructor: function (document) {

      let parsed = DomParser.parse(document);
      let parsedLayers = parsed.getElementsByTagName("Layer");
      let layers = parsedLayers.map(layer => {
        let name = layer.getElementsByTagName("Title")[0].childNodes[0].nodeValue;
        let id = layer.getElementsByTagName("Identifier")[0].childNodes[0].nodeValue;
        let lowerCorner = layer.getElementsByTagName("ows:LowerCorner")[0].childNodes[0].nodeValue;
        let upperCorner = layer.getElementsByTagName("ows:UpperCorner")[0].childNodes[0].nodeValue;
        let boundingbox = [...lowerCorner.split(" "), ...upperCorner.split(" ")];
        boundingbox = boundingbox.map(value => parseFloat(value));

        return {
          name: name,
          id: id,
          boundingbox: boundingbox
        }

      });

      this._layers = layers;

      this.getLayers = function() {
        return this._layers;
      }

      this.getLayerById = function (id) {

      };

      this.getLayerCenter = function (id) {
        let layer = this._layers.find(l => {
          if (id == l.id) {
            return l;
          }
        });

        return {
          lon: (layer.boundingbox[0] + layer.boundingbox[2]) / 2,
          lat: (layer.boundingbox[1] + layer.boundingbox[3]) / 2
        };

      };


    }

  });

});
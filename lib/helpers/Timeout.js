define([
  "dojo/_base/declare"
], function (declare) {

  return declare(null, {

    constructor: function (duration, callback) {
      this._duration = duration;
      this._callback = callback;
      this._timeoutHandle = null;
    },

    trigger: function () {
      clearTimeout(this._timeoutHandle);
      this._timeoutHandle = setTimeout(this._callback, this._duration);
    },

    cancel: function () {
      clearTimeout(this._timeoutHandle);
    }

  });

});
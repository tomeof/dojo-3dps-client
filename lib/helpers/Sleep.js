define([
  "dojo/_base/declare"
], function (declare) {

  return declare(null, {

    constructor: function () {

      this.sleep = function (milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
          currentDate = Date.now();
        } while (currentDate - date < milliseconds);

      };

    }

  });

});
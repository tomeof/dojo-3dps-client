define(function () {
  return {

    apply: function (vertices, xOffset, yOffset, zOffset) {
      for (let i = 0, len = vertices.length; i < len; i += 3) {
        vertices[i] += xOffset;
        vertices[i + 1] += yOffset;
        vertices[i + 2] += zOffset;
      }
    }

  };

});
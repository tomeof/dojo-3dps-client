define([
  "dojo/_base/declare",
  "CesiumAdaptor/CesiumViewer"
], function (declare, CesiumViewer) {

  return declare(null, {

    constructor: function () {
      this._viewer = CesiumViewer.getViewer();
    },

    cullingVolume2Url: function () {
      let planes = this._viewer.camera.frustum._offCenterFrustum._cullingVolume.planes;
      let result = [];
      for (i = 0, len = planes.length; i < len; i++) {
        result.push(planes[i].x)
        result.push(planes[i].y)
        result.push(planes[i].z)
        result.push(planes[i].w)
      }
      return result.join(",");
    },

    camera2Url: function () {
      let camera = this._viewer.camera;
      let position = camera.position;
      let direction = camera.direction;
      let result = [];
      result.push(position.x);
      result.push(position.y);
      result.push(position.z);
      result.push(direction.x);
      result.push(direction.y);
      result.push(direction.z);
      return result.join(",");
    },

    frustum2Url: function () {
      let near = this._viewer.camera.frustum._offCenterFrustum.near;
      let top = this._viewer.camera.frustum._offCenterFrustum.top;
      let right = this._viewer.camera.frustum._offCenterFrustum.right;
      let result = [];
      result.push(near);
      result.push(top);
      result.push(right);
      return result.join(",");
    },

    drawingBuffer2Url: function () {
      let width = this._viewer.scene.frameState.context.drawingBufferWidth;
      let height = this._viewer.scene.frameState.context.drawingBufferHeight;
      let result = [];
      result.push(width);
      result.push(height);
      return result.join(",");
    }

  });

});
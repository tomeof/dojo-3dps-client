define(function () {
  return {

    apply: function (cartesianArray, typedArray) {
      for (let i = 0, len = cartesianArray.length; i < len; i++) {
        typedArray[i * 3] = cartesianArray[i].x;
        typedArray[i * 3 + 1] = cartesianArray[i].y;
        typedArray[i * 3 + 2] = cartesianArray[i].z;
      }
    }

  };

});
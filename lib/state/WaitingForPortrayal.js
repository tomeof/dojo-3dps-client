define([
  "dojo/_base/declare"
], function (declare) {

  return declare(null, {

    constructor: function () { },

    init(target, bb) {
      $('#toggleFreezeScene').prop('disabled', true);
    },

    settingBoundingBox: function (target, bb) {
      // no-op
    },

    clearingBoundingBox: function (target, bb) {
      // no-op
    },

    makingRequest: function (target) {
      target.operations.invokeRequest();
    },

    toggleFreezingScene: function (target) {
      // no-op
    },

    selectingLayer: function (target, layer) {
      // no-op
    },

    gettingLayers: function (target) {
      // no-op
    },

    showAddTempLayer: function (target) {
      // no-op
    },

    addTempLayer: function (target) {
      // no-op
    }

  });

});
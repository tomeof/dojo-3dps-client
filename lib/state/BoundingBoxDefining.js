define([
  "dojo/_base/declare",
], function (declare) {

  return declare(null, {

    constructor: function () {

    },

    init(target, bb) {
      bb.register();
      $("#boundingBoxInput").val("");
      $('#toggleFreezeScene').prop('checked', false);
      $('#toggleFreezeScene').prop('disabled', true);
    },

    settingBoundingBox: function (target, bb) {
      bb.clear();
    },

    clearingBoundingBox: function (target, bb) {
      // no-op
    },

    makingRequest: function (target) {
      // no-op
    },

    toggleFreezingScene: function (target) {
      // no-op
    },

    selectingLayer: function (target, layer) {
      // no-op
    },

    gettingLayers: function (target) {
      // no-op
    },

    showAddTempLayer: function (target) {
      // no-op
    },

    addTempLayer: function (target) {
      // no-op
    }


  });

});
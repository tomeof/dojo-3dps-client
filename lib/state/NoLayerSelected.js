define([
  "dojo/_base/declare",
  "helpers/NodeCache"
], function (declare, NodeCache) {

  return declare(null, {

    constructor: function () {

    },

    init(target, bb) {
      bb.clear();
      NodeCache.clearAll();
      target.operations.clear();
      $("#boundingBoxInput").val("");
      $('#toggleFreezeScene').prop('checked', false);
      $('#toggleFreezeScene').prop('disabled', true);
    },

    settingBoundingBox: function (target, bb) {
      // no-op
    },

    clearingBoundingBox: function (target, bb) {
      // no-op
    },

    makingRequest: function (target) {
      // no-op
    },

    toggleFreezingScene: function (target) {
      // no-op
    },

    selectingLayer: function (target, layer) {
      target.operations.clear();
      target.operations.selectLayer(layer);
      target.changeState("LayerSelected");
    },

    gettingLayers: function (target) {
      target.operations.getLayers();
      target.changeState("NoLayerSelected");
    },

    showAddTempLayer: function (target) {
      target.operations.showAddTempLayer();
    },

    addTempLayer: function (target) {
      target.operations.addTempLayer();
    }

  });

});
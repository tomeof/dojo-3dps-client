define([
  "dojo/_base/declare",
  "helpers/NodeCache"
], function (declare, NodeCache) {

  return declare(null, {

    constructor: function () {

    },

    init(target, bb) {
      // no-op
    },

    settingBoundingBox: function (target, bb) {
      bb.clear();
      // NodeCache.clearAll();  
      target.changeState("BoundingBoxDefining");
    },

    clearingBoundingBox: function (target, bb) {
      if (bb.isDefined()) {
        target.operations.clearBoundingBox();
        target.changeState("LayerSelected");
        target.makeRequest();
      }
    },

    makingRequest: function (target) {
      // no-op
    },

    toggleFreezingScene: function (target) {
      target.changeState("UnfreezeScene");
    },

    selectingLayer: function (target, layer) {
      target.operations.clear();
      target.operations.selectLayer(layer);
      target.changeState("LayerSelected");
    },

    gettingLayers: function (target) {
      target.operations.getLayers();
      target.changeState("NoLayerSelected");
    },

    showAddTempLayer: function (target) {
      target.operations.showAddTempLayer();
    },

    addTempLayer: function (target) {
      target.operations.addTempLayer();
    }

  });

});
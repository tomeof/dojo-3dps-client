define([
  "dojo/_base/declare",
  "helpers/NodeCache"
], function (declare, NodeCache) {

  return declare(null, {

    constructor: function () { },

    init(target, bb) {
      bb.unregister();
      NodeCache.clearAll();
      $("#boundingBoxInput").val(bb.toString());
      $('#toggleFreezeScene').prop('checked', false);
      $("#toggleFreezeScene").prop("disabled", false);
      target.operations.setBoundingBox(bb.toString());
      target.makeRequest();
    },

    settingBoundingBox: function (target, bb) {
      bb.clear();
      NodeCache.clearAll();
      target.changeState("BoundingBoxDefining");
    },

    clearingBoundingBox: function (target, bb) {
      // no-op
    },

    makingRequest: function (target) {
      target.operations.invokeRequest();
      target.changeState("WaitingForPortrayal");
    },

    toggleFreezingScene: function (target) {
      target.changeState("FreezeScene");
    },

    selectingLayer: function (target, layer) {
      target.operations.selectLayer(layer);
      target.changeState("LayerSelected");
    },

    gettingLayers: function (target) {
      target.operations.getLayers();
      target.changeState("NoLayerSelected");
    },

    showAddTempLayer: function (target) {
      target.operations.showAddTempLayer();
    },

    addTempLayer: function (target) {
      target.operations.addTempLayer();
    }


  });

});
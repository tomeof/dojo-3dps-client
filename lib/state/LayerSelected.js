define([
  "dojo/_base/declare",
  "helpers/NodeCache"
], function (declare, NodeCache) {

  return declare(null, {

    constructor: function () {

    },

    init(target, bb) {
      bb.clear();
      NodeCache.clearAll();
      $("#boundingBoxInput").val("");
      $('#toggleFreezeScene').prop('checked', false);
      $('#toggleFreezeScene').prop('disabled', true);
    },

    settingBoundingBox: function (target, bb) {
      // no-op
    },

    clearingBoundingBox: function (target, bb) {
      // no-op
    },

    makingRequest: function (target) {
      target.operations.invokeRequest();
      target.changeState("WaitingForPortrayal");
    },

    toggleFreezingScene: function (target) {
      // no-op
    },

    selectingLayer: function (target, layer) {
      target.operations.selectLayer(layer);
    },

    gettingLayers: function (target) {
      target.operations.getLayers();
      target.changeState("NoLayerSelected");
    },

    showAddTempLayer: function (target) {
      // no-op
    },

    addTempLayer: function (target) {
      // no-op
    }

  });

});
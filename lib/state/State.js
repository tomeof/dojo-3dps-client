define([
  "dojo/_base/declare",
  "dojo/topic",
  "helpers/BoundingBox",
  "state/StateFactory"
], function (declare, topic, BoundingBox, StateFactory) {

  return declare(null, {

    constructor: function (operations) {
      this.operations = operations;
      this._stateFactory = new StateFactory();
      this._current = this._stateFactory.createState("NothingSelected");
      this._boundingBox = new BoundingBox();
      topic.subscribe("BoundingBoxCreated", this.changeState.bind(this));
      topic.subscribe("PortrayalResponseHandled", this.changeState.bind(this));
      topic.subscribe("TempLayerAdded", this.changeState.bind(this));
    },

    setBoundingBox: function () {
      this._current.settingBoundingBox(this, this._boundingBox);
    },

    clearBoundingBox: function () {
      this._current.clearingBoundingBox(this, this._boundingBox);
    },

    makeRequest: function () {
      this._current.makingRequest(this);
    },

    changeState: function (newStateName) {
      this._current = this._stateFactory.createState(newStateName);
      this._current.init(this, this._boundingBox);
    },

    toggleFreezeScene: function () {
      this._current.toggleFreezingScene(this);
    },

    getBB: function () {
      return this._boundingBox;
    },

    selectLayer: function (layer) {
      this._current.selectingLayer(this, layer);
    },

    getLayers: function () {
      this._current.gettingLayers(this);
    },

    showAddTempLayer: function () {
      this._current.showAddTempLayer(this);
    },

    addTempLayer: function () {
      this._current.addTempLayer(this);
    }

  });

});
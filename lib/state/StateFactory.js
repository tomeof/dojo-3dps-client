define([
  "dojo/_base/declare",
  "state/NothingSelected",
  "state/NoLayerSelected",
  "state/LayerSelected",
  "state/BoundingBoxUndefined",
  "state/BoundingBoxDefining",
  "state/BoundingBoxDefined",
  "state/WaitingForPortrayal",
  "state/FreezeScene",
  "state/UnfreezeScene",
  "state/SceneRendered",
  "state/ExceptionReturned",
  "state/TemporaryLayerAdded"
], function (
  declare,
  NothingSelected,
  NoLayerSelected,
  LayerSelected,
  BoundingBoxUndefined,
  BoundingBoxDefining,
  BoundingBoxDefined,
  WaitingForPortrayal,
  FreezeScene,
  UnfreezeScene,
  SceneRendered,
  ExceptionReturned,
  TemporaryLayerAdded
) {

  return declare(null, {

    constructor: function () {

    },

    createState: function (stateName) {
      switch (stateName) {
        case "NothingSelected":
          return new NothingSelected();
        case "NoLayerSelected":
          return new NoLayerSelected();
        case "LayerSelected":
          return new LayerSelected();
        case "BoundingBoxUndefined":
          return new BoundingBoxUndefined();
        case "BoundingBoxDefining":
          return new BoundingBoxDefining();
        case "BoundingBoxDefined":
          return new BoundingBoxDefined();
        case "WaitingForPortrayal":
          return new WaitingForPortrayal();
        case "FreezeScene":
          return new FreezeScene();
        case "UnfreezeScene":
          return new UnfreezeScene();
        case "SceneRendered":
          return new SceneRendered();
        case "ExceptionReturned":
          return new ExceptionReturned();
        case "TemporaryLayerAdded":
          return new TemporaryLayerAdded();
        default:
          return new NoLayerSelected();
      }
    }

  });

});
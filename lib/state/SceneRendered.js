define([
  "dojo/_base/declare",
  "helpers/NodeCache",
  "helpers/RequestWrapper"
], function (declare, NodeCache, RequestWrapper) {

  return declare(null, {

    constructor: function () { },

    init(target, bb) {
      $('#toggleFreezeScene').prop('disabled', false);
    },

    settingBoundingBox: function (target, bb) {
      bb.clear();
      target.changeState("BoundingBoxDefining");
    },

    clearingBoundingBox: function (target, bb) {
      if (bb.isDefined()) {
        bb.clear();
        NodeCache.clearAll();
        target.operations.clearBoundingBox();
        $("#boundingBoxInput").val("");
        target.makeRequest();
      }

    },

    makingRequest: function (target) {
      target.operations.invokeRequest();
      target.changeState("WaitingForPortrayal");
    },

    toggleFreezingScene: function (target) {
      target.changeState("FreezeScene");
    },

    selectingLayer: function (target, layer) {
      target.operations.clear();
      target.operations.selectLayer(layer);
      target.changeState("LayerSelected");
    },

    gettingLayers: function (target) {
      target.operations.getLayers();
      target.changeState("NoLayerSelected");
    },

    showAddTempLayer: function (target) {
      target.operations.showAddTempLayer();
    },

    addTempLayer: function (target) {
      target.operations.addTempLayer();
    }


  });

});
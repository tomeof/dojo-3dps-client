define([
  "dojo/_base/declare",
  "helpers/NodeCache"
], function (declare, NodeCache) {

  return declare(null, {

    constructor: function () { },

    init(target, bb) {
      // bb.unregister();
      NodeCache.clearAll();
      // $("#boundingBoxInput").val(bb.toString());
      $('#toggleFreezeScene').prop('checked', false);
      $("#toggleFreezeScene").prop("disabled", false);
      // target.operations.setBoundingBox(bb.toString());
      target.makeRequest();
    },

    settingBoundingBox: function (target, bb) {
      // no-op
    },

    clearingBoundingBox: function (target, bb) {
      // no-op
    },

    makingRequest: function (target) {
      target.operations.invokeRequest();
      target.changeState("WaitingForPortrayal");
    },

    toggleFreezingScene: function (target) {
      target.changeState("FreezeScene");
    },

    selectingLayer: function (target, layer) {
      // no-op
    },

    gettingLayers: function (target) {
      // no-op
    },

    showAddTempLayer: function (target) {
      // no-op
    },

    addTempLayer: function (target) {
      // no-op
    }

  });

});
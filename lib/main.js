require([
  "dojo/topic",
  "dojo/_base/config",
  "CesiumAdaptor/CesiumViewer",
  "helpers/NodeCache",
  "style/NodeStyler",
  "style/DefaultStyle",
  "style/NodeLevelStyle",
  "gui/Legend",
  "helpers/Timeout",
  "state/State",
  "state/Operations",
  "dojo/domReady!"
], function (topic, config, CesiumViewer, NodeCache, NodeStyler, DefaultStyle, NodeLevelStyle, Legend, Timeout, State, Operations) {

  Cesium.BingMapsApi.defaultKey = "AmdoJEIiP8BcA__QO9SjBAgv4h73-uiH4lGinmzwyfjK-Fjs8t7GbmfpFjmyj1cl";
  let osm = Cesium.createOpenStreetMapImageryProvider({
    url: "https://a.tile.openstreetmap.org/"
  });

  CesiumViewer.createViewer("cesiumContainer", {
    baseLayerPicker: true,
    imageryProvider: osm,
    fullscreenButton: false,
    scene3DOnly: true,
    timeline: false,
    animation: false,
    selectionIndicator: false,
    infoBox: false
  });

  let legend = new Legend("wrapper", "i3sNodeLevelLegend");
  legend.setRight("20px");
  legend.setBottom("60px");
  legend.setTitle("Node Level");
  legend.addEntry(Cesium.Color.ORANGE.toBytes().slice(0, -1), "1");
  legend.addEntry(Cesium.Color.BLUEVIOLET.toBytes().slice(0, -1), "2");
  legend.addEntry(Cesium.Color.CYAN.toBytes().slice(0, -1), "3");
  legend.addEntry(Cesium.Color.GREENYELLOW.toBytes().slice(0, -1), "4");
  legend.addEntry(Cesium.Color.DEEPPINK.toBytes().slice(0, -1), "5");
  legend.addEntry(Cesium.Color.BLUE.toBytes().slice(0, -1), "6");
  legend.addEntry(Cesium.Color.RED.toBytes().slice(0, -1), "7");
  legend.setVisible(false);
  legend.render();

  let operations = new Operations();
  let state = new State(operations);

  $("#host").val(config.app.host);
  $("#port").val(config.app.port);
  $("#endpoint").val(config.app.endpoint);

  $("#request").click(function (event) {
    event.preventDefault();
    state.makeRequest();

  });

  $("#layersList").change(function (event) {
    state.selectLayer(this.value);
  });

  $("#stylesList").change(function () {

    let selection = this.value;
    switch (selection) {
      case "Default":
        NodeStyler.setStyle(new DefaultStyle());
        NodeCache.updateStyle();
        legend.hide();
        break;
      case "Node Level":
        NodeStyler.setStyle(new NodeLevelStyle());
        NodeCache.updateStyle();
        legend.show();
        break;
      default:
        NodeStyler.setStyle(new DefaultStyle());
    }

  });

  $("#setBoundingBox").click(function (event) {
    state.setBoundingBox();
  });

  $("#clearBoundingBox").click(function (event) {
    state.clearBoundingBox();
  });

  $("#toggleFreezeScene").change(function () {
    state.toggleFreezeScene();
  });

  let timeout = new Timeout(1000,
    function () {
      state.makeRequest();
    }
  );

  topic.subscribe("CameraChanged", function (event) {
    timeout.trigger();
  });

  $("#getLayers").click(function (event) {
    state.getLayers();
  });

  $("#showAddTempLayer").click(function (event) {
    state.showAddTempLayer();
  });

  $("#addTempLayer").click(function (event) {
    state.addTempLayer();
  });

  $("#cancelTempLayer").click(function (event) {
    $("#tempLayerName").val("");
    $("#tempLayerUrl").val("");
  });

});

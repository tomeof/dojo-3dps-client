define([
  'style/DefaultStyle'
], function (DefaultStyle) {

  let _style = new DefaultStyle();

  return {

    setStyle: function (newStyle) {
      _style = newStyle;
    },
    getColor: function (nodeLevel) {
      return _style.getColor(nodeLevel);
    }
  };

});
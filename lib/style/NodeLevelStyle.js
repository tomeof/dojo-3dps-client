define([
  "dojo/_base/declare"
], function (declare) {

  return declare(null, {

    constructor: function () { },

    getColor: function (nodeLevel) {
      let result;
      switch (true) {
        case nodeLevel == 1:
          result = Cesium.Color.ORANGE;
          break;
        case nodeLevel == 2:
          result = Cesium.Color.BLUEVIOLET;
          break;
        case nodeLevel == 3:
          result = Cesium.Color.CYAN;
          break;
        case nodeLevel == 4:
          result = Cesium.Color.GREENYELLOW;
          break;
        case nodeLevel == 5:
          result = Cesium.Color.DEEPPINK;
          break;
        case nodeLevel == 6:
          result = Cesium.Color.BLUE;
          break;
        case nodeLevel == 7:
          result = Cesium.Color.RED;
          break;
        default:
          result = Cesium.Color.DARKGREY;
      }

      return result;
    }

  });

});
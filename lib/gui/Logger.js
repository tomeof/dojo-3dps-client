define([
], function () {

  function Logger(div) {
    this.logLevel = {
      "i": "info",
      "e": "error"
    };
    this.style = {
      "i": "bg-info",
      "e": "bg-danger"
    };

    this.binding = div;
    this.log = function (level, message) {
      let time = new Date().toString().split(" ")[4];
      this.binding.append('<p class="' + this.style[level] + '">['
        + this.logLevel[level] + "] "
        + "[" + time + "] " + message);
      this.binding.scrollTop(this.binding[0].scrollHeight);
    };
  }

});
define([
  "dojo/_base/declare"
], function (declare) {

  return declare(null, {

    constructor: function (anchor, id) {
      this._anchor = anchor;
      this._id = id;
      this._topGroupId = this._id + "-top-group";
      this._bottomGroupId = this._id + "-bottom-group";
      this._isTopGroupUsed = false;
      this._isBottomGroupUsed = false;
      this._title;
      this._subtitle;
      this._endnote;
      this._entries = [];
      this._location = {
        top: "initial",
        bottom: "initial",
        left: "initial",
        right: "initial"
      };
      this._visible = "initial";

      this.setTop = function (value) {
        this._location.top = value;
      };

      this.setBottom = function (value) {
        this._location.bottom = value;
      };

      this.setLeft = function (value) {
        this._location.left = value;
      };

      this.setRight = function (value) {
        this._location.right = value;
      };

      this.setTitle = function (text) {
        this._title = text;
        this._isTopGroupUsed = true;
      };

      this.setSubtitle = function (text) {
        this._subtitle = text;
        this._isTopGroupUsed = true;
      };

      this.setEndnote = function (text) {
        this._endnote = text;
        this._isBottomGroupUsed = true;
      };

      this.setVisible = function (value) {
        if (!value) {
          this._visible = "none";
        }
      };

      this.show = function () {
        $("#" + this._id).css({
          "display": "initial"
        });
      };

      this.hide = function () {
        $("#" + this._id).css({
          "display": "none"
        });
      };

      this.addEntry = function (rgb, label) {
        this._entries.push({ "rgb": rgb, "label": label });
      };

      this.applyStyle = function () {

        $("#" + this._id).addClass("legend");

        $("#" + this._id).css({
          "top": this._location.top,
          "bottom": this._location.bottom,
          "left": this._location.left,
          "right": this._location.right,
          "display": this._visible
        });
      };

      this.render = function () {
        $("#" + this._anchor).append('<div id="' + this._id + '"></div>');

        if (this._isTopGroupUsed) {
          $("#" + this._id).append('<div id="' + this._topGroupId + '" class="legend-top-group"></div>');

          if (this._title) {
            $("#" + this._topGroupId).append('<div class="legend-title">' + this._title + '</div>');
          }

          if (this._subtitle) {
            $("#" + this._topGroupId).append('<div class="legend-subtitle">' + this._subtitle + '</div>');
          }
        }

        for (let i = 0, len = this._entries.length; i < len; i++) {
          let symbolHtml = '<div class="legend-symbol" style="background-color:rgb(' + this._entries[i].rgb.join(",") + ')"></div>';
          let labelHtml = '<div class="legend-label">' + this._entries[i].label + '</div>';
          $("#" + this._id).append('<div class="legend-entry">' + symbolHtml + labelHtml + '</div>');
        }

        if (this._isBottomGroupUsed) {
          $("#" + this._id).append('<div id="' + this._bottomGroupId + '" class="legend-bottom-group"></div>');
          $("#" + this._bottomGroupId).append('<div class="legend-endnote">' + this._endnote + '</div>');
        }

        this.applyStyle();
      };

    }// constructor

  });

});
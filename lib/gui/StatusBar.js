define([

], function () {

  return {

    beginProcess: function (message) {
      if ($("#statusBarLoader").is(":hidden")) { $("#statusBarLoader").show(); }
      $("#statusBarMessage").html(message + "...")
    },

    endProcess: function () {
      $("#statusBarLoader").hide();
      $("#statusBarMessage").empty();
    },

    display: function (message) {
      $("#statusBarMessage").text(message)
    }

  };

});
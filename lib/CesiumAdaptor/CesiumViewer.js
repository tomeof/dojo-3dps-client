define([
  "dojo/topic"
], function (topic) {

  let _viewer = null;

  return {

    createViewer: function (elementId, opts) {
      if (_viewer != null) {
        return;
      }

      _viewer = new Cesium.Viewer(elementId, opts);

      // register callback
      _viewer.camera.changed.addEventListener(function () {
        topic.publish("CameraChanged", {
          message: "camera changed",
          time: Date.now()
        });
      });

    },

    getViewer: function () {
      return _viewer;
    },

    flyTo: function (lon, lat) {
      if (_viewer == null) {
        return;
      }
      _viewer.camera.flyTo({
        destination: Cesium.Cartesian3.fromDegrees(lon, lat, 3000.0),
        duration: 0.0
      });
    }

  };

});
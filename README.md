# Dojo 3D Portrayal Client 

![](figures/new-york.jpg)
This is a 3D Portrayal Service client that is compatible with an [extended](https://gitlab.com/tomeof/node-3dpsx) version of the [3D Portrayal Service](https://www.ogc.org/standards/3dp) which can consume an [i3s](https://github.com/Esri/i3s-spec) layer in [Cesium](https://cesium.com) globe.


## Installing

The client is implemented using the [Dojo Toolkit](https://dojotoolkit.org). Clone or download the repository and place the contents in a folder and use a web server to publish it.

## Usage

You can alter the default host, port and service endpoint. Click on "Get Layers" to get the server capabilities. Click on "Add layer" to add a temporary i3s layer. The expiry duration can be found in the config.js file on the server side (default is 24 hours). Pick a layer from the layers list. Then, just navigate in the scene. Click on the "Set" button to define a bounding box for additional spatial filtering and pick two points on the Cesium viewer.  If you freeze the scene, then no additional requests to the service are made while you navigate the scene.

Layers available in the default host:
* Hamburg buildings
* Berlin buildings
* New York buildings
* Portland buildings
* Boston buildings
* Geneva buildings

## Demo

[http://81.169.187.7:3201](http://81.169.187.7:3201)